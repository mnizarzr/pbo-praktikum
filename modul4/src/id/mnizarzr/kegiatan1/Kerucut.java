package id.mnizarzr.kegiatan1;

public class Kerucut extends BangunRuang {

    double t, r, s;

    public Kerucut(double t, double r) {
        this.t = t;
        this.r = r;
        this.s = Math.sqrt(Math.pow(r, 2) + Math.pow(t, 2));
    }

    public double getT() {
        return t;
    }

    public void setT(double t) {
        this.t = t;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        this.s = Math.sqrt(Math.pow(r, 2) + Math.pow(t, 2));
    }

    @Override
    public double getLuasPermukaan() {
        double la = Math.PI * Math.pow(r, 2);
        double ls = Math.PI * r * s;

        return la + ls;
    }

    @Override
    public double getVolume() {
        return ((double) 1 / 3) * Math.PI * Math.pow(r, 2) * t;
    }
}
