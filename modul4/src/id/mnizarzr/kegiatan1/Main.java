package id.mnizarzr.kegiatan1;

public class Main {

    public static void main(String[] args) {

        Bola bola = new Bola(14);
        Kerucut kerucut = new Kerucut(24, 7);

        System.out.println("Luas permukaan bola: " + bola.getLuasPermukaan());
        System.out.println("Volume bola: " + bola.getVolume());

        System.out.println("Luas permukaan kerucut: " + kerucut.getLuasPermukaan());
        System.out.println("Volume kerucut: " + kerucut.getVolume());

    }

}
