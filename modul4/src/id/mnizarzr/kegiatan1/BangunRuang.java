package id.mnizarzr.kegiatan1;

public abstract class BangunRuang {

    public abstract double getLuasPermukaan();

    public abstract double getVolume();

}
