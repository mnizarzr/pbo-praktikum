package id.mnizarzr.kegiatan2;

public class Job {

    String id;
    String name;
    String desc;
    String dept;
    int salary;

    public Job(String id, String name, String desc, String dept, int salary) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.dept = dept;
        this.salary = salary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", salary=" + salary +
                '}';
    }
}
