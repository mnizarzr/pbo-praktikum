package id.mnizarzr.kegiatan2;

public class Main {

    public static void main(String[] args) {

        Job[] job = new Job[2];
        job[0] = new Job("TECH_PM", "Project Manager", "Lorem ipsum dolor sit amet", "TECH", 5000);
        job[1] = new Job("HR_HRD", "Human Resource Development", "Lorem ipsum dolor sit amet", "HR", 2000);

        Employee emp1 = new Employee("PM020", "Kang Pe Em", "Jalan lorem ipsum", 28, job[0]);
        Employee emp2 = new Employee("HR001", "Kang Ha Er", "Jalan dolor sit amet", 32, job[1]);

        System.out.println(emp1);
        System.out.println(emp2);

    }

}
