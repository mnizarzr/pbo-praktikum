package id.mnizarzr.kegiatan3.model;

public class Tank extends Hero {

    final double upHp = 200, upDefense = 15, upAttack = 20;

    public Tank(int level) {
        super(level);
        if (level <= 0) {
            throw new IllegalArgumentException("Level must be above 0");
        }
        this.healthPoint = 7000;
        this.defense = 500;
        this.attackDamage = 500;
        this.ups(upHp, upDefense, upAttack);
        this.heroType = this.getClass().getSimpleName();
    }

    @Override
    public void spawnIntro() {
        System.out.println("Tranktanktanktanktank");
    }


}
