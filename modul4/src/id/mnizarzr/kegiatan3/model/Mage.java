package id.mnizarzr.kegiatan3.model;

import id.mnizarzr.kegiatan3.IMagicalDamage;

public class Mage extends Hero implements IMagicalDamage {

    final double upHp = 85, upDefense = 10, upAttack = 35;

    public Mage(int level) {
        super(level);
        if (level <= 0) {
            throw new IllegalArgumentException("Level must be above 0");
        }
        this.healthPoint = 2500;
        this.defense = 200;
        this.attackDamage = 700;
        this.attackDamage += this.attackDamage * magicDamageBonus;
        this.ups(upHp, upDefense, upAttack);

        this.heroType = this.getClass().getSimpleName();
    }

    @Override
    public void spawnIntro() {
        System.out.println("I'm a mage from Hogwarts");
    }

}
