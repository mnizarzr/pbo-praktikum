package id.mnizarzr.kegiatan3.model;

public abstract class Hero {

    double healthPoint, attackDamage, defense;
    int level;
    boolean isAlive;
    String heroType;

    public Hero(int level) {
        this.level = level;
        isAlive = true;
    }

    public double getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(double healthPoint) {
        this.healthPoint = healthPoint;
    }

    public double getAttackDamage() {
        return attackDamage;
    }

    public void setAttackDamage(double attackDamage) {
        this.attackDamage = attackDamage;
    }

    public double getDefense() {
        return defense;
    }

    public void setDefense(double defense) {
        this.defense = defense;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        this.isAlive = alive;
    }

    public void attack(Hero target) {
        target.reviewDamage(this.attackDamage);
    }

    public void reviewDamage(double damage) {
        double realDamage = damage - this.getDefense();
        this.healthPoint -= realDamage;
        if (this.healthPoint <= 0) {
            this.setHealthPoint(0);
            this.setAlive(false);
        }
    }

    public String getHeroType() {
        return heroType;
    }

    public abstract void spawnIntro();

    public void ups(double healthPoint, double defense, double attackDamage) {
        for (int lvl = 1; lvl <= this.getLevel(); lvl++) {
            setHealthPoint(getHealthPoint() + healthPoint);
            setDefense(getDefense() + defense);
            setAttackDamage(getAttackDamage() + attackDamage);
        }
    }

    public void checkStatus() {
        System.out.println(this.getHeroType());
        System.out.println("Level: " + this.getLevel());
        System.out.println("Attack: " + this.getAttackDamage());
        System.out.println("Defense: " + this.getDefense());
        System.out.println("Health: " + this.getHealthPoint());
        System.out.println("Life Status: " + this.isAlive());
    }
}
