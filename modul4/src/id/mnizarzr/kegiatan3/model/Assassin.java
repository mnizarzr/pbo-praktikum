package id.mnizarzr.kegiatan3.model;

import id.mnizarzr.kegiatan3.ICriticalDamage;

public class Assassin extends Hero implements ICriticalDamage {

    final double upHp = 90, upDefense = 15, upAttack = 30;

    public Assassin(int level) {
        super(level);
        if (level <= 0) {
            throw new IllegalArgumentException("Level must be above 0");
        }
        this.healthPoint = 3000;
        this.defense = 300;
        this.attackDamage = 800;
        this.attackDamage += attackDamage * bonusDamage;

        this.ups(upHp, upDefense, upAttack);

        this.heroType = this.getClass().getSimpleName();
    }

    @Override
    public void spawnIntro() {
        System.out.println("I'm an assassin");
    }

}
