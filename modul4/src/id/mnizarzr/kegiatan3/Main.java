package id.mnizarzr.kegiatan3;

import id.mnizarzr.kegiatan3.model.Assassin;
import id.mnizarzr.kegiatan3.model.Mage;

public class Main {

    public static void main(String[] args) {

        Assassin player1 = new Assassin(8);
        Mage player2 = new Mage(5);

        System.out.println("======Player 1======");
        player1.checkStatus();
        System.out.println();
        System.out.println("======Player 2======");
        player2.checkStatus();

        System.out.println();
        System.out.println("======FIGHT BEGIN======");
        int i = 1;
        while (player1.isAlive() && player2.isAlive()) {
            System.out.println("======Round " + i + "======");

            System.out.println("======Player 1 Turn======");
            player1.spawnIntro();
            player1.attack(player2);
            System.out.println("Player 2 HP: " + player2.getHealthPoint());

            if (!player2.isAlive()) break;

            System.out.println();

            System.out.println("======Player 2 Turn======");
            player2.spawnIntro();
            player2.attack(player1);
            System.out.println("Player 1 HP: " + player1.getHealthPoint());
            if (!player1.isAlive()) break;
            i++;
        }

        if (!player1.isAlive()) {
            System.out.println("Player 1 is dead");
            System.out.println("Player 2 WIN");
        }
        if (!player2.isAlive()) {
            System.out.println("Player 2 is dead");
            System.out.println("Player 1 WIN");
        }

    }

}
