package id.mnizarzr.modul2.kegiatan1;

public class Perhitungan {

    private int nilaiProgdas;
    private int nilaiKalkulus;
    private int nilaiOrkom;
    private double hasil;

    public Perhitungan(int nilaiProgdas, int nilaiKalkulus, int nilaiOrkom) {
        this.nilaiProgdas = nilaiProgdas;
        this.nilaiKalkulus = nilaiKalkulus;
        this.nilaiOrkom = nilaiOrkom;
    }

    public int getNilaiProgdas() {
        return nilaiProgdas;
    }

    public void setNilaiProgdas(int nilaiProgdas) {
        this.nilaiProgdas = nilaiProgdas;
    }

    public int getNilaiKalkulus() {
        return nilaiKalkulus;
    }

    public void setNilaiKalkulus(int nilaiKalkulus) {
        this.nilaiKalkulus = nilaiKalkulus;
    }

    public int getNilaiOrkom() {
        return nilaiOrkom;
    }

    public void setNilaiOrkom(int nilaiOrkom) {
        this.nilaiOrkom = nilaiOrkom;
    }

    public double getHasil() {
        return hasil;
    }

    public void setHasil() {
        this.hasil = getNilaiProgdas() + getNilaiOrkom() + getNilaiKalkulus();
    }

    public double hitungRata() {
        return getHasil() / 3;
    }

    public void cekNilai() {
        if (hitungRata() > 70) {
            System.out.println("LULUS");
        } else {
            System.out.println("GAGAL");
        }
    }

}
