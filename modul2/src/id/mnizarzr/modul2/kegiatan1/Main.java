package id.mnizarzr.modul2.kegiatan1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    private final static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int progdas;
        int kalkulus;
        int orkom;

        while (true) {
            try {
                System.out.print("Nilai Prodas: ");
                progdas = scanner.nextInt();
                System.out.print("Nilai Kalkulus: ");
                kalkulus = scanner.nextInt();
                System.out.print("Nilai Orkom: ");
                orkom = scanner.nextInt();
                break;
            } catch (InputMismatchException inputMismatchException) {
                System.out.println("Nilai masukan tidak berupa angka");
                scanner.next();
                System.out.println("\n");
            }
        }

        Perhitungan perhitungan = new Perhitungan(progdas, kalkulus, orkom);
        System.out.println();
        System.out.println("Nilai Anda: ");
        System.out.println("Nilai Prodas: " + perhitungan.getNilaiProgdas());
        System.out.println("Nilai Kalkulus: " + perhitungan.getNilaiKalkulus());
        System.out.println("Nilai Orkom: " + perhitungan.getNilaiOrkom());
        perhitungan.setHasil();
        System.out.println();
        System.out.println("Rata-rata nilai: " + perhitungan.hitungRata());
        System.out.print("Status: ");
        perhitungan.cekNilai();

    }
}
