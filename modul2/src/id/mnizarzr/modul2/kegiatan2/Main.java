package id.mnizarzr.modul2.kegiatan2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    private final static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int choice;

        while (true) {
            try {
                System.out.println("Pilihan:");
                System.out.println("1. Hitung keliling belah ketupat");
                System.out.println("2. Hitung luas belah ketupat");
                System.out.print("Pilih [1 atau 2]: ");
                choice = scanner.nextInt();
                if (choice == 1 || choice == 2) {
                    switch (choice) {
                        case 1:
                            hitungKelilingBelahKetupat();
                            break;
                        case 2:
                            hitungLuasBelahKetupat();
                            break;
                    }
                    break;
                } else {
                    throw new Exception("Pilihan tidak tersedia, masukkan 1 atau 2");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
                System.out.println("\n");
            }

        }

    }

    private static void hitungLuasBelahKetupat() {

        float d1;
        float d2;

        while (true) {
            try {
                System.out.print("Diagonal 1: ");
                d1 = scanner.nextFloat();
                System.out.print("Diagonal 2: ");
                d2 = scanner.nextFloat();

                BelahKetupat belahKetupat = new BelahKetupat(d1, d2);
                float hasil = (float) (0.5 * belahKetupat.getD1() * belahKetupat.getD2());
                System.out.println("Hasil: " + hasil);
                break;
            } catch (InputMismatchException inputMismatchException) {
                System.out.println("Nilai masukan tidak berupa angka");
                scanner.next();
                System.out.println("\n");
            }
        }
    }

    private static void hitungKelilingBelahKetupat() {
        float sisi;

        while (true) {
            try {
                System.out.print("Panjang sisi: ");
                sisi = scanner.nextFloat();

                BelahKetupat belahKetupat = new BelahKetupat(sisi);
                float hasil = 4 * belahKetupat.getSisi();
                System.out.println("Hasil: " + hasil);
                break;
            } catch (InputMismatchException inputMismatchException) {
                System.out.println("Nilai masukan tidak berupa angka");
                scanner.next();
                System.out.println("\n");
            }
        }
    }

}
