package id.mnizarzr.modul2.kegiatan2;

public class BelahKetupat {

    private float d1;
    private float d2;
    private float sisi;

    public BelahKetupat(float d1, float d2) {
        this.d1 = d1;
        this.d2 = d2;
    }

    public BelahKetupat(float sisi) {
        this.sisi = sisi;
    }

    public float getD1() {
        return d1;
    }

    public void setD1(float d1) {
        this.d1 = d1;
    }

    public float getD2() {
        return d2;
    }

    public void setD2(float d2) {
        this.d2 = d2;
    }

    public float getSisi() {
        return sisi;
    }

    public void setSisi(float sisi) {
        this.sisi = sisi;
    }
}
