package id.mnizarzr;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.daftar();

    }
}

class Mahasiswa {

    private String name;
    private String nim;
    private String password;
    private final Scanner scanner = new Scanner(System.in);

    public Mahasiswa() {
    }

    public String getName() {
        return name;
    }

    public void setName() {
        while (true) {
            String name = null;
            System.out.print("NAMA : ");
            name = scanner.nextLine();
            if (name.isBlank()) {
                System.out.println("Nama tidak boleh kosong");
            } else {
                this.name = name;
                break;
            }
        }
    }

    public String getNim() {
        return nim;
    }

    public void setNim() {
        while (true) {
            String nim = null;
            System.out.print("NIM : ");
            nim = scanner.nextLine();
            if (nim.length() != 15) {
                System.out.println("NIM harus persis 15 karakter");
            } else {
                this.nim = nim;
                break;
            }
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword() {
        while (true) {
            String password = null;
            System.out.print("PASSWORD : ");
            password = scanner.nextLine();
            if (password.length() < 8) {
                System.out.println("Password minimal 8 karakter");
            } else {
                this.password = password;
                break;
            }
        }
    }

    public void daftar() {

        System.out.println("🎓== DAFTAR ==🎓");
        this.setName();
        this.setNim();
        this.setPassword();

    }

}