package id.mnizarzr.modulenam;

import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.controls.MFXTextField;
import io.github.palexdev.materialfx.enums.FloatMode;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;

import java.io.IOException;
import java.util.Objects;
import java.util.function.UnaryOperator;

public class MainApplication extends Application {

    private final TableView<Jadwal>      table        = new TableView<>();
    private       ObservableList<Jadwal> data         = FXCollections.observableArrayList();
    private       VBox                   vBox         = new VBox();
    private       HBox                   hBox         = new HBox();
    private       HBox                   hboxField    = new HBox();
    private       HBox                   hboxButton   = new HBox();
    private final MFXTextField           dosenField   = new MFXTextField();
    private final MFXTextField           matkulField  = new MFXTextField();
    private final MFXTextField           gedungField  = new MFXTextField();
    private final MFXTextField           waktuField   = new MFXTextField();
    private final MFXTextField           ruanganField = new MFXTextField();
    private       MFXButton              createButton = new MFXButton("Create");
    private       MFXButton              editButton   = new MFXButton("Edit");
    private       MFXButton              deleteButton = new MFXButton("Delete");
    private       TableColumn            namaDosenCol = new TableColumn("Nama Dosen");
    private       TableColumn            matkulCol    = new TableColumn("Mata Kuliah");
    private       TableColumn            gedungCol    = new TableColumn("GKB");
    private       TableColumn            waktuCol     = new TableColumn("Waktu");
    private       TableColumn            ruanganCol   = new TableColumn("Ruangan");

    @Override
    public void start(Stage stage) throws IOException {
        stage.setTitle("Tugas Modul 6");
        Scene scene = new Scene(new Group());
        Label label = new Label("Jadwal");
        label.setFont(Font.font("Oxygen", FontWeight.MEDIUM, 50));
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("css/style.css")).toExternalForm());

        vBox.setAlignment(Pos.CENTER);
        vBox.setPadding(new Insets(10, 10, 10, 10));
        vBox.setSpacing(10);
        vBox.getChildren().add(label);
        setupTable();
        setupFieldsButtons();
        hBox.setSpacing(20);
        hBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(hBox);

        ((Group) scene.getRoot()).getChildren().add(vBox);

        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    private void setupTable() {

        namaDosenCol.setMinWidth(200);
        namaDosenCol.setCellValueFactory(new PropertyValueFactory<Jadwal, String>("namaDosen"));

        matkulCol.setMinWidth(200);
        matkulCol.setCellValueFactory(new PropertyValueFactory<Jadwal, String>("matkul"));

        waktuCol.setMinWidth(200);
        waktuCol.setCellValueFactory(new PropertyValueFactory<Jadwal, String>("waktu"));

        gedungCol.setMinWidth(200);
        gedungCol.setCellValueFactory(new PropertyValueFactory<Jadwal, String>("gedung"));

        ruanganCol.setMinWidth(200);
        ruanganCol.setCellValueFactory(new PropertyValueFactory<Jadwal, String>("ruangan"));

        table.setEditable(true);
        table.getColumns().addAll(namaDosenCol,
                                  matkulCol,
                                  gedungCol,
                                  waktuCol,
                                  ruanganCol);


        table.setItems(data);

        vBox.getChildren().add(table);

    }

    private void setupFieldsButtons() {

        MFXTextField[] fields = {dosenField, matkulField, gedungField, waktuField, ruanganField};

        hboxField.setSpacing(10);
        hboxButton.setSpacing(10);

        dosenField.setFloatMode(FloatMode.BORDER);
        matkulField.setFloatMode(FloatMode.BORDER);
        gedungField.setFloatMode(FloatMode.BORDER);
        waktuField.setFloatMode(FloatMode.BORDER);
        ruanganField.setFloatMode(FloatMode.BORDER);
        dosenField.setFloatingText("Nama dosen");
        matkulField.setFloatingText("Mata kuliah");
        gedungField.setFloatingText("GKB");
        waktuField.setFloatingText("Waktu");
        ruanganField.setFloatingText("Ruangan");

        UnaryOperator<TextFormatter.Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("([1-9][0-9]*)?")) {
                return change;
            }
            return null;
        };

        gedungField.delegateSetTextFormatter(new TextFormatter<>(new IntegerStringConverter(), 1, integerFilter));

        hboxField.getChildren().addAll(dosenField, matkulField, gedungField, waktuField, ruanganField);
        hBox.getChildren().add(hboxField);

        createButton.setStyle("-fx-background-color: #7a0ed9;");
        editButton.setStyle("-fx-background-color: #4caf50;");
        deleteButton.setStyle("-fx-background-color: #EF6E6B;");
        createButton.setPrefHeight(38);
        editButton.setPrefHeight(38);
        deleteButton.setPrefHeight(38);

        Alert errorEmptyAlert = new Alert(Alert.AlertType.ERROR, "Inputan tidak boleh ada yang kosong");
        errorEmptyAlert.setTitle("Error empty data");

        createButton.setOnAction(event -> {
            if (checkEmpty()) {
                errorEmptyAlert.show();
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Data telah ditambahkan");
                alert.show();
                data.add(new Jadwal(dosenField.getText(),
                                    matkulField.getText(),
                                    gedungField.getText(),
                                    waktuField.getText(),
                                    ruanganField.getText()));

                for (MFXTextField field : fields) field.clear();

            }
        });

        editButton.setOnAction(event -> {
            if (checkEmpty()) {
                errorEmptyAlert.show();
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Data telah diperbarui");
                alert.show();
                var index = table.getSelectionModel().getSelectedIndex();
                data.set(index, new Jadwal(dosenField.getText(),
                                    matkulField.getText(),
                                    gedungField.getText(),
                                    waktuField.getText(),
                                    ruanganField.getText()));
                for (MFXTextField field : fields) field.clear();
            }
        });

        deleteButton.setOnAction(event -> {
            table.getItems().removeAll(table.getSelectionModel().getSelectedItem());
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Data telah dihapus");
            alert.show();
        });

        hboxButton.getChildren().addAll(createButton, editButton, deleteButton);
        hBox.getChildren().add(hboxButton);

    }

    private boolean checkEmpty() {
        return dosenField.getText().isBlank()
                || matkulField.getText().isBlank()
                || gedungField.getText().isBlank()
                || waktuField.getText().isBlank()
                || ruanganField.getText().isBlank();
    }

    public static void main(String[] args) {
        launch();
    }
}