package id.mnizarzr.modulenam;

import javafx.beans.property.SimpleStringProperty;

public class Jadwal {

    private final SimpleStringProperty namaDosen, matkul, waktu, gedung, ruangan;

    Jadwal(String namaDosen, String matkul, String waktu, String gedung, String ruangan) {
        this.namaDosen = new SimpleStringProperty(namaDosen);
        this.matkul    = new SimpleStringProperty(matkul);
        this.waktu     = new SimpleStringProperty(waktu);
        this.gedung    = new SimpleStringProperty(gedung);
        this.ruangan   = new SimpleStringProperty(ruangan);
    }

    public String getNamaDosen() {
        return namaDosen.get();
    }

    public SimpleStringProperty namaDosenProperty() {
        return namaDosen;
    }

    public void setNamaDosen(String namaDosen) {
        this.namaDosen.set(namaDosen);
    }

    public String getMatkul() {
        return matkul.get();
    }

    public SimpleStringProperty matkulProperty() {
        return matkul;
    }

    public void setMatkul(String matkul) {
        this.matkul.set(matkul);
    }

    public String getWaktu() {
        return waktu.get();
    }

    public SimpleStringProperty waktuProperty() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu.set(waktu);
    }

    public String getGedung() {
        return gedung.get();
    }

    public SimpleStringProperty gedungProperty() {
        return gedung;
    }

    public void setGedung(String gedung) {
        this.gedung.set(gedung);
    }

    public String getRuangan() {
        return ruangan.get();
    }

    public SimpleStringProperty ruanganProperty() {
        return ruangan;
    }

    public void setRuangan(String ruangan) {
        this.ruangan.set(ruangan);
    }
}
