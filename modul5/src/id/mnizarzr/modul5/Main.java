package id.mnizarzr.modul5;

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        String fileName = "dinaspertanahan.txt";
        String filePath = System.getenv("USERPROFILE") + "\\" + fileName;
        DinasPertanahan dp = new DinasPertanahan();
        PrintWriter printWriter = null;
        boolean re = true;

        try {
            FileWriter fileWriter = new FileWriter(filePath, true);
            printWriter = new PrintWriter(fileWriter);

            do {
                String alamat;
                int panjang, luas;
                System.out.print("Masukkan alamat: ");
                alamat = scanner.nextLine();
                System.out.print("Masukkan Panjang: ");
                panjang = Integer.parseInt(scanner.nextLine());
                System.out.print("Masukkan Luas: ");
                luas = Integer.parseInt(scanner.nextLine());

                dp.setAlamat(alamat);
                dp.setPanjang(panjang);
                dp.setLuas(luas);

                printWriter.printf("%s,%s,%s\n", dp.getAlamat(), dp.getPanjang(), dp.getLuas());

                System.out.print("Input lagi? [y/n] = ");
                re = scanner.next().equals("y");
                scanner.nextLine();
            } while (re);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Kalo ngakses kejauhan ngab");
        } catch (IOException | InputMismatchException e) {
            System.out.println(e.getMessage());
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Tidak berupa angka");
        } finally {
            assert printWriter != null;
            printWriter.close();
        }

        try (FileReader fr = new FileReader(filePath)) {

            BufferedReader br = new BufferedReader(fr);
            int i = 0;
            String line;
            String[] separated;
            while ((line = br.readLine()) != null) {
                separated = line.split(",");
                System.out.println("\nData ke-" + (i + 1));
                System.out.println("Alamat: " + separated[0]);
                System.out.println("Panjang Tanah: " + separated[1]);
                System.out.println("Luas Tanah: " + separated[2]);
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}