package id.mnizarzr.modul5;

public class DinasPertanahan {

    private String alamat;
    private int panjang, luas;

    public DinasPertanahan() {
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public int getLuas() {
        return luas;
    }

    public void setLuas(int luas) {
        this.luas = luas;
    }

    @Override
    public String toString() {
        return "id.mnizarzr.modul5.DinasPertanahan{" +
                "alamat='" + alamat + '\'' +
                ", panjang=" + panjang +
                ", luas=" + luas +
                '}';
    }
}
