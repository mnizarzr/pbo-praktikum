package id.mnizarzr.modulenamez;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApp extends Application {
    private TableView<Mahasiswa>      table = new TableView<>();
    final   ObservableList<Mahasiswa> data  = FXCollections.observableArrayList();

    @Override
    public void start(Stage stage) throws IOException {
        Scene scene1 = new Scene(new Group());
        Scene scene2 = new Scene(new Group());
        stage.setTitle("Easy");
        final Label label = new Label("Daftar Mahasiswa");
        label.setFont(new Font("Arial", 30));
        table.setEditable(true);
        TableColumn<Mahasiswa, String> nameCol     = new TableColumn<>("Nama");
        TableColumn<Mahasiswa, String> nimCol      = new TableColumn<>("NIM");
        TableColumn<Mahasiswa, String> emailCol    = new TableColumn<>("Email");
        TableColumn<Mahasiswa, String> fakultasCol = new TableColumn<>("Fakultas");
        TableColumn<Mahasiswa, String> jurusanCol  = new TableColumn<>("Jurusan");
        TableColumn<Mahasiswa, String> alamatCol   = new TableColumn<>("Alamat");
        TableColumn<Mahasiswa, String> kotaCol     = new TableColumn<>("Kota");

        nameCol.setCellValueFactory(
                new PropertyValueFactory<>("name")
                                   );
        nimCol.setCellValueFactory(
                new PropertyValueFactory<>("nim")
                                  );
        emailCol.setCellValueFactory(
                new PropertyValueFactory<>("email")
                                    );
        fakultasCol.setCellValueFactory(
                new PropertyValueFactory<>("fakultas")
                                       );
        jurusanCol.setCellValueFactory(
                new PropertyValueFactory<>("jurusan")
                                      );
        alamatCol.setCellValueFactory(
                new PropertyValueFactory<>("alamat")
                                     );
        kotaCol.setCellValueFactory(
                new PropertyValueFactory<>("kota")
                                   );
        table.setItems(data);

        table.getColumns().addAll(
                nameCol,
                nimCol,
                emailCol,
                fakultasCol,
                jurusanCol,
                alamatCol,
                kotaCol
                                 );

        final VBox vbox1 = new VBox();
        final VBox vbox2 = new VBox();

        vbox1.setSpacing(8);
        vbox1.setPadding(new Insets(20, 10, 10, 10));
        vbox2.setSpacing(8);
        vbox2.setPadding(new Insets(20, 10, 10, 10));

        final TextField nameField     = new TextField();
        final TextField nimField      = new TextField();
        final TextField emailField    = new TextField();
        final TextField fakultasField = new TextField();
        final TextField jurusanField  = new TextField();
        final TextField alamatField   = new TextField();
        final TextField kotaField     = new TextField();

        nameField.setMaxWidth(nameCol.getPrefWidth());
        nameField.setPromptText("Nama Mahasiswa");

        nimField.setMaxWidth(nimCol.getPrefWidth());
        nimField.setPromptText("NIM");

        emailField.setMaxWidth(emailCol.getPrefWidth());
        emailField.setPromptText("Email");

        fakultasField.setMaxWidth(emailCol.getPrefWidth());
        fakultasField.setPromptText("Fakultas");

        jurusanField.setMaxWidth(emailCol.getPrefWidth());
        jurusanField.setPromptText("Jurusan");

        alamatField.setMaxWidth(emailCol.getPrefWidth());
        alamatField.setPromptText("Alamat");

        kotaField.setMaxWidth(emailCol.getPrefWidth());
        kotaField.setPromptText("Kota");

        TextField[] fields = {nameField, nimField, emailField, fakultasField, jurusanField, alamatField, kotaField};

        final Alert  errorFieldAlert = new Alert(Alert.AlertType.ERROR);
        final Alert  errorNimAlert   = new Alert(Alert.AlertType.ERROR);
        final Alert  errorEmailAlert = new Alert(Alert.AlertType.ERROR);
        final Button addButton       = new Button("Add");
        final Button backButton      = new Button("Back");
        final Button showButton      = new Button("Show");

        addButton.setOnAction(e -> {

            boolean isValid = true;

            if (nameField.getText().isBlank()
                    || nimField.getText().isBlank()
                    || emailField.getText().isBlank()
                    || fakultasField.getText().isBlank()
                    || jurusanField.getText().isBlank()
                    || alamatField.getText().isBlank()
                    || kotaField.getText().isBlank()) {
                errorFieldAlert.setContentText("Inputan tidak boleh kosong");
                errorFieldAlert.show();
                isValid = false;
            }

            try {
                Integer.parseInt(nimField.getText());
            } catch (NumberFormatException numberFormatException) {
                errorNimAlert.setContentText("NIM harus berupa angka");
                errorNimAlert.show();
                nimField.clear();
                isValid = false;
            }

            if (!emailField.getText().endsWith("@webmail.umm.ac.id")) {
                isValid = false;
                errorEmailAlert.setContentText("Email harus berakhiran @webmail.umm.ac.id");
                errorEmailAlert.show();
            }

            if (isValid) {
                data.add(new Mahasiswa(
                        nameField.getText(),
                        nimField.getText(),
                        emailField.getText(),
                        fakultasField.getText(),
                        jurusanField.getText(),
                        alamatField.getText(),
                        kotaField.getText()
                ));
                stage.setScene(scene2);
            }

            for (TextField field : fields) {
                field.clear();
            }

        });

        backButton.setOnAction(event -> {
            stage.setScene(scene1);
        });

        showButton.setOnAction(event -> {
            stage.setScene(scene2);
        });

        vbox1.getChildren().add(label);

        vbox1.getChildren().addAll(
                nameField,
                nimField,
                emailField,
                fakultasField,
                jurusanField,
                alamatField,
                kotaField,
                addButton,
                showButton
                                  );

        vbox2.getChildren().addAll(table, backButton);


        ((Group) scene1.getRoot()).getChildren().addAll(vbox1);
        ((Group) scene2.getRoot()).getChildren().addAll(vbox2);
        stage.setScene(scene1);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}