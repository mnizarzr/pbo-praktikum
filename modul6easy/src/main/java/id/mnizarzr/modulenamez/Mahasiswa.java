package id.mnizarzr.modulenamez;

import javafx.beans.property.SimpleStringProperty;

public class Mahasiswa {
    private final SimpleStringProperty name, nim, email, fakultas, jurusan, alamat, kota;

    Mahasiswa(String name, String nim, String email, String fakultas, String jurusan, String alamat, String kota) {
        this.name     = new SimpleStringProperty(name);
        this.nim      = new SimpleStringProperty(nim);
        this.email    = new SimpleStringProperty(email);
        this.fakultas = new SimpleStringProperty(fakultas);
        this.jurusan  = new SimpleStringProperty(jurusan);
        this.alamat   = new SimpleStringProperty(alamat);
        this.kota     = new SimpleStringProperty(kota);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String fName) {
        name.set(fName);
    }

    public String getNim() {
        return nim.get();
    }

    public void setNim(String fName) {
        nim.set(fName);
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String fName) {
        email.set(fName);
    }

    public String getFakultas() {
        return fakultas.get();
    }

    public void setFakultas(String fakultas) {
        this.fakultas.set(fakultas);
    }

    public String getJurusan() {
        return jurusan.get();
    }


    public void setJurusan(String jurusan) {
        this.jurusan.set(jurusan);
    }

    public String getAlamat() {
        return alamat.get();
    }

    public void setAlamat(String alamat) {
        this.alamat.set(alamat);
    }

    public String getKota() {
        return kota.get();
    }

    public void setKota(String kota) {
        this.kota.set(kota);
    }
}
