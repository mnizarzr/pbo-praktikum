package id.mnizarzr;

import id.mnizarzr.model.*;

public class Main {

    public static void main(String[] args) {
        Assassin assassin = new Assassin("Ezio", 100, 1, true);
        Mage mage = new Mage("Harry Potter", 50, 2, false);
        Tank tank = new Tank("Gibraltar", 100, 500f, false);
        Marksman marksman = new Marksman("Seni perrrr", 80, 200f, true);
        Warrior warrior = new Warrior("Si paling", 90, 3, false);

        BaseHero[] heroes = {assassin, mage, tank, marksman, warrior};
        for (BaseHero hero :
                heroes) {
            hero.printGreeting();
            hero.showInfo();
            System.out.println();
        }

        assassin.setStealthMode(false);
        System.out.println("===> Assassin stealth mode: " + assassin.isStealthMode());
        System.out.println();

        mage.setMightyMode(true);
        System.out.println("Mage mighty mode: " + mage.isMightyMode());
        System.out.println();

        tank.setShieldMode(true);
        System.out.println("Tank shield mode: " + tank.isShieldMode());
        System.out.println();

        marksman.setAimMode(false);
        System.out.println("Marksman aim mode: " + marksman.isAimMode());
        System.out.println();

        warrior.setBerserkerMode(true);
        System.out.println("Warrior berserker mode: " + warrior.isBerserkerMode());
        System.out.println();

    }

}
