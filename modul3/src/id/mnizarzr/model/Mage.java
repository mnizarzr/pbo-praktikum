package id.mnizarzr.model;

import id.mnizarzr.BaseHero;

public class Mage extends BaseHero {

    boolean isMightyMode;

    public Mage(String name, int health, int level, float exp, boolean isMightyMode) {
        super(name, health, level, exp);
        this.isMightyMode = isMightyMode;
        this.setHeroType("mage");
    }

    public Mage(String name, int health, float exp, boolean isMightyMode) {
        super(name, health, exp);
        this.isMightyMode = isMightyMode;
        this.setHeroType("mage");
    }

    public Mage(String name, int health, int level, boolean isMightyMode) {
        super(name, health, level);
        this.isMightyMode = isMightyMode;
        this.setHeroType("mage");
    }

    public Mage(String name, boolean isMightyMode) {
        super(name);
        this.isMightyMode = isMightyMode;
        this.setHeroType("mage");
    }

    public boolean isMightyMode() {
        return isMightyMode;
    }

    public void setMightyMode(boolean mightyMode) {
        isMightyMode = mightyMode;
        if (isMightyMode)
            System.out.println("🧙🧙🧙🧙🧙🧙");
        else
            System.out.println("👨🏼‍🎓");
    }

    @Override
    protected void printGreeting() {
        System.out.println("==============================");
        super.printGreeting();
        System.out.println("I'm a mage, you can call me " + this.getName() + " of Hogwarts");
        System.out.println("==============================");
    }

}
