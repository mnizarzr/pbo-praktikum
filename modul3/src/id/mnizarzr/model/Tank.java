package id.mnizarzr.model;

import id.mnizarzr.BaseHero;

public class Tank extends BaseHero {

    boolean isShieldMode;

    public Tank(String name, int health, int level, float exp, boolean isShieldMode) {
        super(name, health, level, exp);
        this.isShieldMode = isShieldMode;
        this.setHeroType("tank");
    }

    public Tank(String name, int health, float exp, boolean isShieldMode) {
        super(name, health, exp);
        this.isShieldMode = isShieldMode;
        this.setHeroType("tank");
    }

    public Tank(String name, int health, int level, boolean isShieldMode) {
        super(name, health, level);
        this.isShieldMode = isShieldMode;
        this.setHeroType("tank");
    }

    public Tank(String name, boolean isShieldMode) {
        super(name);
        this.isShieldMode = isShieldMode;
        this.setHeroType("tank");
    }

    public boolean isShieldMode() {
        return isShieldMode;
    }

    public void setShieldMode(boolean shieldMode) {
        isShieldMode = shieldMode;
        if (isShieldMode) {
            System.out.println("You can't break the barrier");
        } else {
            System.out.println("I need to drink");
        }
    }

    @Override
    protected void printGreeting() {
        System.out.println("==============================");
        super.printGreeting();
        System.out.println("Tranktanktanktanktank");
        System.out.println("==============================");
    }

}
