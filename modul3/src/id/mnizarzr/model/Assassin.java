package id.mnizarzr.model;

import id.mnizarzr.BaseHero;

public class Assassin extends BaseHero {

    boolean isStealthMode;

    public Assassin(String name, int health, int level, float exp, boolean isStealthMode) {
        super(name, health, level, exp);
        this.isStealthMode = isStealthMode;
        this.setHeroType("assassin");
    }

    public Assassin(String name, int health, float exp, boolean isStealthMode) {
        super(name, health, exp);
        this.isStealthMode = isStealthMode;
        this.setHeroType("assassin");
    }

    public Assassin(String name, int health, int level, boolean isStealthMode) {
        super(name, health, level);
        this.isStealthMode = isStealthMode;
        this.setHeroType("assassin");
    }

    public Assassin(String name, boolean isStealthMode) {
        super(name);
        this.isStealthMode = isStealthMode;
        this.setHeroType("assassin");
    }

    public boolean isStealthMode() {
        return isStealthMode;
    }

    public void setStealthMode(boolean stealthMode) {
        isStealthMode = stealthMode;
        if (isStealthMode) {
            System.out.println("I'm not here");
        } else {
            System.out.println("Peek-a-boo!!");
        }
    }

    @Override
    protected void printGreeting() {
        System.out.println("==============================");
        super.printGreeting();
        System.out.println("My name is " + this.getName() + ", I'm an assassin");
        System.out.println("==============================");
    }

}
