package id.mnizarzr.model;

import id.mnizarzr.BaseHero;

public class Warrior extends BaseHero {

    boolean isBerserkerMode;

    public Warrior(String name, int health, int level, float exp, boolean isBerserkerMode) {
        super(name, health, level, exp);
        this.isBerserkerMode = isBerserkerMode;
        this.setHeroType("warrior");
    }

    public Warrior(String name, int health, float exp, boolean isBerserkerMode) {
        super(name, health, exp);
        this.isBerserkerMode = isBerserkerMode;
        this.setHeroType("warrior");
    }

    public Warrior(String name, int health, int level, boolean isBerserkerMode) {
        super(name, health, level);
        this.isBerserkerMode = isBerserkerMode;
        this.setHeroType("warrior");
    }

    public Warrior(String name, boolean isBerserkerMode) {
        super(name);
        this.isBerserkerMode = isBerserkerMode;
        this.setHeroType("warrior");
    }

    public boolean isBerserkerMode() {
        return isBerserkerMode;
    }

    public void setBerserkerMode(boolean berserkerMode) {
        isBerserkerMode = berserkerMode;
        if (isBerserkerMode)
            System.out.println("Imma sharpen my sword with your bones");
        else
            System.out.println("My bed needs me");
    }

    @Override
    protected void printGreeting() {
        System.out.println("==============================");
        super.printGreeting();
        System.out.println("Here we are, don't turn away now,\n" +
                "We are the warriors that built this town");
        System.out.println("==============================");
    }


}
