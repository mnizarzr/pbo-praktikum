package id.mnizarzr.model;

import id.mnizarzr.BaseHero;

public class Marksman extends BaseHero {

    boolean isAimMode;

    public Marksman(String name, int health, int level, float exp, boolean isAimMode) {
        super(name, health, level, exp);
        this.isAimMode = isAimMode;
        this.setHeroType("marksman");
    }

    public Marksman(String name, int health, float exp, boolean isAimMode) {
        super(name, health, exp);
        this.isAimMode = isAimMode;
        this.setHeroType("marksman");
    }

    public Marksman(String name, int health, int level, boolean isAimMode) {
        super(name, health, level);
        this.isAimMode = isAimMode;
        this.setHeroType("marksman");
    }

    public Marksman(String name, boolean isAimMode) {
        super(name);
        this.isAimMode = isAimMode;
        this.setHeroType("marksman");
    }

    public boolean isAimMode() {
        return isAimMode;
    }

    public void setAimMode(boolean aimMode) {
        isAimMode = aimMode;
        if (isAimMode)
            System.out.println("Aiming for your head");
        else
            System.out.println("ughh my vision's blurred");
    }

    @Override
    protected void printGreeting() {
        System.out.println("==============================");
        super.printGreeting();
        System.out.println("👀 " + this.getName() + " 👀");
        System.out.println("==============================");
    }

}
