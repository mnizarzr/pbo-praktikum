package id.mnizarzr;

public class BaseHero {

    private final float cExp = 0.05f;
    private String name;
    private int health;
    private int level;
    private float exp;
    private String heroType = "superhero";

    public BaseHero(String name, int health, int level, float exp) {
        this.name = name;
        this.health = health;
        this.level = level;
        this.exp = exp;
    }

    public BaseHero(String name, int health, float exp) {
        this.name = name;
        this.health = health;
        this.exp = exp;
        this.level = (int) (cExp * Math.sqrt(exp));
    }

    public BaseHero(String name, int health, int level) {
        this.name = name;
        this.health = health;
        this.level = level;
        this.exp = (float) (Math.pow(this.level, 2) / Math.pow(this.cExp, 2));
    }

    public BaseHero(String name) {
        this.name = name;
        this.health = 100;
        this.level = (int) (cExp * Math.sqrt(exp));
        this.exp = (float) (Math.pow(this.level, 2) / Math.pow(this.cExp, 2));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public float getExp() {
        return exp;
    }

    public void setExp(float exp) {
        this.exp = exp;
    }


    public String getHeroType() {
        return heroType;
    }

    public void setHeroType(String heroType) {
        this.heroType = heroType;
    }

    protected void printGreeting() {
        System.out.println("People call me a hero");
    }

    protected void showInfo() {
        System.out.println("Name: " + this.getName());
        System.out.println("Health: " + this.getHealth());
        System.out.println("Level: " + this.getLevel());
        System.out.println("Exp: " + this.getExp());
        System.out.println("Type: " + this.getHeroType());
    }
}
